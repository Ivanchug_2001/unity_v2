﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [Tooltip("bomba que se lanza")]
    public GameObject bomb;
    [Tooltip("Punto donde se generan las bombas")]
    public Transform spawnPosition;

    // Start is called before the first frame update
    void Start()
    {
        Bombard();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Bombard()
    {
        if (bomb != null)
        {
            Instantiate(bomb);
        }
    }
}
