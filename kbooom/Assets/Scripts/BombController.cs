﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{
    [Tooltip("velocidad de caida de la bomba")]
    public float speed = 3f;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        Transform t = GetComponent<Transform>();
        Vector3 pos = t.position;
        pos.y = pos.y - speed * Time.deltaTime;
        t.position = pos;
    }
}